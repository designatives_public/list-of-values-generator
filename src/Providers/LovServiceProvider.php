<?php

namespace Designatives\ListOfValuesGenerator\Providers;

use Illuminate\Support\ServiceProvider;
use Designatives\ListOfValuesGenerator\LovGeneratorCommand;

class LovServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->app->singleton('command.lov.controller', function ($app) {
                return new LovGeneratorCommand($app['files']);
            });

            $this->commands(['command.lov.controller']);
        }
    }

    public function register()
    {

    }
}
